---
AWSTemplateFormatVersion: '2010-09-09'
Description: "Load Balancers para Kubernetes UAT Teste"

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Loadbalancer Configuration
      Parameters:
      - VPC
      - SubnetList
      - K8sNlbName
      - K8sNlbTargetGroupName
      - K8sAlbAdmName
      - K8sAlbAdmTargetGroupName
#      - K8sAlbAdmCertArn
      - K8sAlbAdmSecurityGroupId
      - K8sAlbServicesName
      - K8sAlbServicesTargetGroupName
#      - K8sAlbServicesCertArn
      - K8sAlbServicesSecurityGroupId
      - CreateMigrationObjects

Parameters:
  VPC:
    Description: VPC
    Type: AWS::EC2::VPC::Id
  SubnetList:
    Description: Lista de Subnets onde serao colocados os load balancers (NLB para PrivateLink/VPC Endpoint Services)
    Type: List<AWS::EC2::Subnet::Id>
  K8sNlbName:
    Description: Nome do NLB Microservicos
    Type: String
    Default: MICROSSERVICOS-NLB-K8S-HML
  K8sNlbTargetGroupName:
    Description: Nome do Target Group para o NLB Microservicos
    Type: String
    Default: MICROSSERVICOS-TG-NLB-K8S-HML
  K8sAlbAdmName:
    Description: Nome do Alb Administrativo
    Type: String
    Default: ADMIN-K8S-HML
  K8sAlbAdmTargetGroupName:
    Description: Nome do Target Group para o Alb Administrativo
    Type: String
    Default: ADMIN-K8S-HML
#  K8sAlbAdmCertArn:
#    Description: ARN do Certificado para o ALB Administrativo
#    Type: String
#    Default: -K8S-HML
  K8sAlbAdmSecurityGroupId:
    Description: ID do security group para o ALB Administrativo
    Type: AWS::EC2::SecurityGroup::Id
  K8sAlbServicesName:
    Description: Nome do Alb Microservicos
    Type: String
    Default: MICROSSERVICOS-ALB-K8S-HML
  K8sAlbServicesTargetGroupName:
    Description: Nome do Target Group para o Alb Microservicos
    Type: String
    Default: MICROSSERVICOS-TG-ALB-K8S-HML
#  K8sAlbServicesCertArn:
#    Description: ARN do Certificado para o ALB Microservicos
#    Type: String
#    Default: -K8S-HML
  K8sAlbServicesSecurityGroupId:
    Description: ID do security group para o ALB Microservicos
    Type: AWS::EC2::SecurityGroup::Id
  CreateMigrationObjects:
    Description: Cria objetos para migracao entre novas versoes de cluster
    Type: String
    AllowedValues:
    - 'Yes'
    - 'No'
    Default: 'No'

Conditions:
  CreateMigration:
    Fn::Equals:
    - Ref: CreateMigrationObjects
    - 'Yes'

Resources:
#NLB para PrivateLink/VPC Endpoint Services
  K8sNlb:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: !Ref K8sNlbName
      Type: network
      Scheme: internal
      Subnets:
        !Ref SubnetList
      LoadBalancerAttributes:
      - Key: load_balancing.cross_zone.enabled
        Value: 'true'
      Tags:
      - Key: Name
        Value: !Ref K8sNlbName
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML

  K8sNlbTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sNlb
    Properties:
      Name: !Ref K8sNlbTargetGroupName
      Tags:
      - Key: Name
        Value: !Ref K8sNlbTargetGroupName
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: TCP
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30745
      Protocol: TCP
      TargetType: instance

  K8sNlbListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sNlb
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn: !Ref K8sNlbTargetGroup
      LoadBalancerArn: !Ref K8sNlb
      Port: 30745
      Protocol: TCP

# NLB Migracao
  K8sNlbMigration:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sNlbName}-MIGRATION"
      Type: network
      Scheme: internal
      Subnets:
        !Ref SubnetList
      LoadBalancerAttributes:
      - Key: load_balancing.cross_zone.enabled
        Value: 'true'
      Tags:
      - Key: Name
        Value: !Sub "${K8sNlbName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML

  K8sNlbTargetGroupMigration:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sNlbMigration
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sNlbTargetGroupName}-MIGRATION"
      Tags:
      - Key: Name
        Value: !Sub "${K8sNlbTargetGroupName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: TCP
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30745
      Protocol: TCP
      TargetType: instance

  K8sNlbListenerMigration:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sNlbMigration
    Condition: CreateMigration
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn: !Ref K8sNlbTargetGroupMigration
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      LoadBalancerArn: !Ref K8sNlbMigration
      Port: 30745
      Protocol: TCP

#ALB Administrativo
  K8sAlbAdm:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: !Ref K8sAlbAdmName
      Scheme: internal
      SecurityGroups:
      - !Ref K8sAlbAdmSecurityGroupId
      Subnets:
        !Ref SubnetList
      Tags:
      - Key: Name
        Value: !Ref K8sAlbAdmName
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML

  K8sAlbAdmTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sAlbAdm
    Properties:
      Name: !Ref K8sAlbAdmTargetGroupName
      Tags:
      - Key: Name
        Value: !Ref K8sAlbAdmTargetGroupName
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: HTTP
      HealthCheckPort: 30744
      HealthCheckPath: /healthz
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30744
      Protocol: HTTP
      Matcher:
        HttpCode: 200
      TargetType: instance

  K8sAlbAdmListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sAlbAdm
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn: !Ref K8sAlbAdmTargetGroup
      LoadBalancerArn: !Ref K8sAlbAdm
      Port: 80
      Protocol: HTTP
#      Certificates:
#        - CertificateArn: !Ref K8sAlbAdmCertArn

#ALB Administrativo - Migracao
  K8sAlbAdmMigration:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sAlbAdmName}-MIGRATION"
      Scheme: internal
      SecurityGroups:
      - !Ref K8sAlbAdmSecurityGroupId
      Subnets:
        !Ref SubnetList
      Tags:
      - Key: Name
        Value: !Sub "${K8sAlbAdmName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML

  K8sAlbAdmTargetGroupMigration:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sAlbAdmMigration
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sAlbAdmTargetGroupName}-MIGRATION"
      Tags:
      - Key: Name
        Value: !Sub "${K8sAlbAdmTargetGroupName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: HTTP
      HealthCheckPort: 30744
      HealthCheckPath: /healthz
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30744
      Protocol: HTTP
      Matcher:
        HttpCode: 200
      TargetType: instance

  K8sAlbAdmListenerMigration:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sAlbAdmMigration
    Condition: CreateMigration
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn: !Ref K8sAlbAdmTargetGroupMigration
      LoadBalancerArn: !Ref K8sAlbAdmMigration
      Port: 80
      Protocol: HTTP
#      Certificates:
#        - CertificateArn: !Ref K8sAlbAdmCertArn

#ALB Microservicos
  K8sAlbServices:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: !Ref K8sAlbServicesName
      Scheme: internal
      SecurityGroups:
      - !Ref K8sAlbServicesSecurityGroupId
      Subnets:
        !Ref SubnetList
      Tags:
      - Key: Name
        Value: !Ref K8sAlbServicesName

  K8sAlbServicesTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sAlbServices
    Properties:
      Name: !Ref K8sAlbServicesTargetGroupName
      Tags:
      - Key: Name
        Value: !Ref K8sAlbServicesTargetGroupName
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: HTTP
      HealthCheckPort: 30745
      HealthCheckPath: /healthz
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30745
      Protocol: HTTP
      Matcher:
        HttpCode: 200
      TargetType: instance

  K8sAlbServicesListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sAlbServices
    Properties:
        DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref K8sAlbServicesTargetGroup
        LoadBalancerArn: !Ref K8sAlbServices
        Port: 80
        Protocol: HTTP
#      Certificates:
#        - CertificateArn: !Ref K8sAlbServicesCertArn

#ALB Microservicos - Migracao
  K8sAlbServicesMigration:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sAlbServicesName}-MIGRATION"
      Scheme: internal
      SecurityGroups:
      - !Ref K8sAlbServicesSecurityGroupId
      Subnets:
        !Ref SubnetList
      Tags:
      - Key: Name
        Value: !Sub "${K8sAlbServicesName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML

  K8sAlbServicesTargetGroupMigration:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    DependsOn: K8sAlbServicesMigration
    Condition: CreateMigration
    Properties:
      Name: !Sub "${K8sAlbServicesTargetGroupName}-MIGRATION"
      Tags:
      - Key: Name
        Value: !Sub "${K8sAlbServicesTargetGroupName}-MIGRATION"
      - Key: Build
        Value: CloudFormation
      - Key: Environment
        Value: HML
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: HTTP
      HealthCheckPort: 30745
      HealthCheckPath: /healthz
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      UnhealthyThresholdCount: 2
      VpcId: !Ref VPC
      Port: 30745
      Protocol: HTTP
      Matcher:
        HttpCode: 200
      TargetType: instance

  K8sAlbServicesListenerMigration:
    Type: AWS::ElasticLoadBalancingV2::Listener
    DependsOn: K8sAlbServicesMigration
    Condition: CreateMigration
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn: !Ref K8sAlbServicesTargetGroupMigration
      LoadBalancerArn: !Ref K8sAlbServicesMigration
      Port: 80
      Protocol: HTTP
#      Certificates:
#        - CertificateArn: !Ref K8sAlbServicesCertArn
